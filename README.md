Tiny Task Manager with Symfony 
---------

Install in Linux 

    composer create-project goncziakos/tiny-task-manager [DIRECTORY]
    
    cd [DIRECTORY] 

Configure the environment, set database connection

    cp .env .env.local
    
    edit .env.local

Database migration
    
    php bin/console doctrine:migrations:migrate --no-interaction

Basic data install

    php bin/console doctrine:fixtures:load --append

Create admin user (username and password: superadmin)

    php bin/console fos:user:create superadmin example@example.com superadmin --super-admin

Assets install

    php bin/console ckeditor:install
    
    php bin/console assets:install --symlink

Built-in web server for the test

    php -S localhost:8000 -t public/

Login
   
    http://localhost:8000/admin/login

