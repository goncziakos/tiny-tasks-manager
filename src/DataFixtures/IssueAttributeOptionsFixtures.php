<?php

namespace App\DataFixtures;

use App\Entity\IssuePriority;
use App\Entity\IssueStatus;
use App\Entity\IssueType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class IssueAttributeOptionsFixtures extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['IssueAttributeOptions'];
    }

    public function load(ObjectManager $manager)
    {
        // Base Issue Priority

        $priority = new IssuePriority();
        $priority->setName('Low');
        $manager->persist($priority);

        $priority = new IssuePriority();
        $priority->setName('Normal');
        $manager->persist($priority);

        $priority = new IssuePriority();
        $priority->setName('High');
        $manager->persist($priority);


        // Base Issue Status

        $status = new IssueStatus();
        $status->setName('Todo');
        $manager->persist($status);

        $status = new IssueStatus();
        $status->setName('In progress');
        $manager->persist($status);

        $status = new IssueStatus();
        $status->setName('Problem');
        $manager->persist($status);

        $status = new IssueStatus();
        $status->setName('Done');
        $manager->persist($status);

        $status = new IssueStatus();
        $status->setName('Cancel');
        $manager->persist($status);


        // Base Issue Type

        $type = new IssueType();
        $type->setName('Task');
        $manager->persist($type);

        $type = new IssueType();
        $type->setName('Subtask');
        $manager->persist($type);

        $type = new IssueType();
        $type->setName('Story');
        $manager->persist($type);

        $type = new IssueType();
        $type->setName('Bug');
        $manager->persist($type);

        $type = new IssueType();
        $type->setName('Epic');
        $manager->persist($type);

        $type = new IssueType();
        $type->setName('Incident');
        $manager->persist($type);

        $type = new IssueType();
        $type->setName('Service request');
        $manager->persist($type);

        $type = new IssueType();
        $type->setName('Change');
        $manager->persist($type);

        $type = new IssueType();
        $type->setName('Problem');
        $manager->persist($type);

        $manager->flush();
    }
}
