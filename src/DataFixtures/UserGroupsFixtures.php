<?php

namespace App\DataFixtures;

use App\Entity\UserGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserGroupsFixtures extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['UserGroup'];
    }

    public function load(ObjectManager $manager)
    {
        // Base User Groups

        $userGroup = new UserGroup('Global Project Manager ', [
            'ROLE_SONATA_MEDIA_ADMIN_GALLERY_ALL',
            'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_ALL',
            'ROLE_SONATA_MEDIA_ADMIN_MEDIA_ALL',
            'ROLE_APP_ADMIN_CLIENT_ALL',
            'ROLE_APP_ADMIN_PROJECT_ALL',
            'ROLE_APP_ADMIN_VERSION_ALL',
            'ROLE_APP_ADMIN_PROJECT_HAS_MEDIA_ALL',
            'ROLE_APP_ADMIN_ISSUE_TYPE_ALL',
            'ROLE_APP_ADMIN_ISSUE_STATUS_ALL',
            'ROLE_APP_ADMIN_ISSUE_PRIORITY_ALL',
            'ROLE_APP_ADMIN_ISSUE_ALL',
            'ROLE_APP_ADMIN_ISSUE_HAS_MEDIA_ALL',
            'ROLE_APP_ADMIN_WORKLOG_ALL',
            'ROLE_ADMIN',
            'ROLE_SUPER_MANAGER'
        ]);
        $manager->persist($userGroup);

        $userGroup = new UserGroup('Project Manager', [
            'ROLE_APP_ADMIN_CLIENT_LIST',
            'ROLE_APP_ADMIN_CLIENT_VIEW',
            'ROLE_APP_ADMIN_PROJECT_ALL',
            'ROLE_APP_ADMIN_VERSION_ALL',
            'ROLE_APP_ADMIN_ISSUE_ALL',
            'ROLE_APP_ADMIN_WORKLOG_ALL',
            'ROLE_ADMIN',
            'ROLE_SONATA_USER_ADMIN_USER_LIST',
            'ROLE_SONATA_USER_ADMIN_USER_VIEW',
            'ROLE_SONATA_USER_ADMIN_GROUP_VIEW',
            'ROLE_SONATA_MEDIA_ADMIN_GALLERY_ALL',
            'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_ALL',
            'ROLE_SONATA_MEDIA_ADMIN_MEDIA_ALL',
            'ROLE_APP_ADMIN_ISSUE_TYPE_LIST',
            'ROLE_APP_ADMIN_ISSUE_TYPE_VIEW',
            'ROLE_APP_ADMIN_ISSUE_STATUS_LIST',
            'ROLE_APP_ADMIN_ISSUE_STATUS_VIEW',
            'ROLE_APP_ADMIN_ISSUE_PRIORITY_LIST',
            'ROLE_APP_ADMIN_ISSUE_PRIORITY_VIEW'
        ]);
        $manager->persist($userGroup);

        $userGroup = new UserGroup('Developer', [
            'ROLE_SONATA_MEDIA_ADMIN_GALLERY_ALL',
            'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_ALL',
            'ROLE_SONATA_MEDIA_ADMIN_MEDIA_ALL',
            'ROLE_APP_ADMIN_CLIENT_LIST',
            'ROLE_APP_ADMIN_CLIENT_VIEW',
            'ROLE_APP_ADMIN_PROJECT_LIST',
            'ROLE_APP_ADMIN_PROJECT_VIEW',
            'ROLE_APP_ADMIN_VERSION_LIST',
            'ROLE_APP_ADMIN_VERSION_VIEW',
            'ROLE_APP_ADMIN_PROJECT_HAS_MEDIA_ALL',
            'ROLE_APP_ADMIN_ISSUE_TYPE_LIST',
            'ROLE_APP_ADMIN_ISSUE_TYPE_VIEW',
            'ROLE_APP_ADMIN_ISSUE_STATUS_LIST',
            'ROLE_APP_ADMIN_ISSUE_STATUS_VIEW',
            'ROLE_APP_ADMIN_ISSUE_PRIORITY_LIST',
            'ROLE_APP_ADMIN_ISSUE_PRIORITY_VIEW',
            'ROLE_APP_ADMIN_ISSUE_ALL',
            'ROLE_APP_ADMIN_WORKLOG_ALL',
            'ROLE_ADMIN'
        ]);
        $manager->persist($userGroup);

        $manager->flush();
    }
}
