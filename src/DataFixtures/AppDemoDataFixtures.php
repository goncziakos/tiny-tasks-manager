<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Project;
use App\Entity\Version;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AppDemoDataFixtures extends Fixture implements FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return ['AppDemoData'];
    }

    public function load(ObjectManager $manager)
    {

        $client = new Client();
        $client->setName('Demo Client 1');
        $manager->persist($client);

        $project = new Project();
        $project->setName('Demo Project 1');
        $project->setClient($client);

        $version = new Version();
        $version->setName('1.0.0');
        $manager->persist($version);

        $project->addVersion($version);
        $manager->persist($project);

        $project = new Project();
        $project->setName('Demo Project 2');
        $project->setClient($client);
        $manager->persist($project);

        $manager->flush();
    }
}
