<?php

namespace App\Repository;

use App\Entity\IssuePriority;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IssuePriority|null find($id, $lockMode = null, $lockVersion = null)
 * @method IssuePriority|null findOneBy(array $criteria, array $orderBy = null)
 * @method IssuePriority[]    findAll()
 * @method IssuePriority[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IssuePriorityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IssuePriority::class);
    }
}
