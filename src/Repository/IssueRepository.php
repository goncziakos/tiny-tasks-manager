<?php

namespace App\Repository;

use App\Entity\Issue;
use App\Model\ProjectRepositoryInterface;
use App\Model\UserInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Issue|null find($id, $lockMode = null, $lockVersion = null)
 * @method Issue|null findOneBy(array $criteria, array $orderBy = null)
 * @method Issue[]    findAll()
 * @method Issue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IssueRepository extends ServiceEntityRepository implements ProjectRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Issue::class);
    }

    public function setProjectUserFilter(QueryBuilder $queryBuilder, UserInterface $user): void
    {
        $alias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->addSelect('project');
        $queryBuilder->join($alias . '.project', 'project');
        $queryBuilder->join('project.users', 'projectUser', Join::WITH, 'projectUser.id = ' . $user->getId());
    }
}
