<?php

namespace App\Repository;

use App\Entity\IssueStatus;
use App\Model\ProjectRepositoryInterface;
use App\Model\UserInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IssueStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method IssueStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method IssueStatus[]    findAll()
 * @method IssueStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IssueStatusRepository extends ServiceEntityRepository implements ProjectRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IssueStatus::class);
    }

    public function setProjectUserFilter(QueryBuilder $queryBuilder, UserInterface $user): void
    {
        $alias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->addSelect('project')
            ->leftJoin($alias . '.project', 'project')
            ->leftJoin('project.users', 'projectUser')
            ->orWhere($queryBuilder->expr()->eq('projectUser.id', $user->getId()))
            ->orWhere($queryBuilder->expr()->isNull($alias . '.project'))
        ;
    }
}
