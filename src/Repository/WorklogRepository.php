<?php

namespace App\Repository;

use App\Entity\Worklog;
use App\Model\ProjectRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Model\UserInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Worklog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Worklog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Worklog[]    findAll()
 * @method Worklog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorklogRepository extends ServiceEntityRepository implements ProjectRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Worklog::class);
    }

    /**
     * @param UserInterface $user
     * @return Worklog|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserLastWorklog(UserInterface $user): ?Worklog
    {
        $query = $this->createQueryBuilder("w")
            ->andWhere('w.assign = :user')
            ->setParameter('user', $user)
            ->addOrderBy('w.id', 'desc')
            ->setMaxResults(1)
            ->getQuery();

        try {
            return $query->getSingleResult();
        } catch (NoResultException $exception) {
            return null;
        }
    }

    public function setProjectUserFilter(QueryBuilder $queryBuilder, UserInterface $user): void
    {
        $alias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->addSelect('issue');
        $queryBuilder->addSelect('project');
        $queryBuilder->addSelect('assign');
        $queryBuilder->join($alias . '.assign', 'assign');
        $queryBuilder->join($alias . '.issue', 'issue');
        $queryBuilder->join('issue.project', 'project');
        $queryBuilder->join('project.users', 'projectUser', Join::WITH, 'projectUser.id = ' . $user->getId());
    }
}
