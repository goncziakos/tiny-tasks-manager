<?php

namespace App\Repository;

use App\Entity\Project;
use App\Model\ProjectRepositoryInterface;
use App\Model\UserInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository implements ProjectRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function setProjectUserFilter(QueryBuilder $queryBuilder, UserInterface $user): void
    {
        $alias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->join($alias . '.users', 'projectUser', Join::WITH, 'projectUser.id = ' . $user->getId());
    }
}
