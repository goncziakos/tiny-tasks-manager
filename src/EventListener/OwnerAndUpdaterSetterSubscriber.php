<?php
/**
 * CreatorUpdaterSetterSubscriber.php
 *
 *
 * @package App\EventListener
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2018.12.31.
 *
 */

namespace App\EventListener;

use App\Model\OwnerAndUpdaterInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Model\UserInterface;

class OwnerAndUpdaterSetterSubscriber implements EventSubscriber
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist,
            Events::preUpdate,
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $user = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;

        if ($entity instanceof OwnerAndUpdaterInterface and $user instanceof UserInterface) {
            $entity->setOwner($user);
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $user = $this->tokenStorage->getToken()->getUser();

        if ($entity instanceof OwnerAndUpdaterInterface and $user instanceof UserInterface) {
            $entity->setUpdater($user);
        }
    }
}