<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181231213921 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql("
INSERT INTO `issue_priority` (`id`, `owner`, `name`, `slug`, `created_at`, `updated_at`, `description`) VALUES
(1, NULL, 'Low', 'low', '2018-12-31 22:21:19', '2018-12-31 22:21:19', NULL),
(2, NULL, 'Normal', 'normal', '2018-12-31 22:21:24', '2018-12-31 22:21:24', NULL),
(3, NULL, 'High', 'high', '2018-12-31 22:21:54', '2018-12-31 22:21:54', NULL)");
        $this->addSql("
INSERT INTO `issue_status` (`id`, `project`, `owner`, `updater`, `name`, `slug`, `created_at`, `updated_at`, `description`) VALUES
(1, NULL, NULL, NULL, 'Todo', 'todo', '2018-12-31 22:11:08', '2018-12-31 22:11:08', NULL),
(2, NULL, NULL, NULL, 'In progress', 'in-progress', '2018-12-31 22:11:20', '2018-12-31 22:11:20', NULL),
(3, NULL, NULL, NULL, 'Done', 'done', '2018-12-31 22:11:30', '2018-12-31 22:11:30', NULL),
(4, NULL, NULL, NULL, 'Cancel', 'cancel', '2018-12-31 22:21:02', '2018-12-31 22:21:02', NULL)");
        $this->addSql("
INSERT INTO `issue_type` (`id`, `project`, `owner`, `updater`, `name`, `slug`, `created_at`, `updated_at`, `description`) VALUES
(1, NULL, NULL, NULL, 'Task', 'task', '2018-12-31 22:16:03', '2018-12-31 22:16:03', 'Task that needs to be done'),
(2, NULL, NULL, NULL, 'Subtask', 'subtask', '2018-12-31 22:16:29', '2018-12-31 22:16:29', 'Smaller task within a larger piece of work'),
(3, NULL, NULL, NULL, 'Story', 'story', '2018-12-31 22:17:13', '2018-12-31 22:17:13', 'Functionality request expressed from the perspective of the user'),
(4, NULL, NULL, NULL, 'Bug', 'bug', '2018-12-31 22:17:28', '2018-12-31 22:17:28', 'Problem that impairs product or service functionality'),
(5, NULL, NULL, NULL, 'Epic', 'epic', '2018-12-31 22:17:48', '2018-12-31 22:17:48', 'Large piece of work that encompasses many issues'),
(6, NULL, NULL, NULL, 'Incident', 'incident', '2018-12-31 22:18:15', '2018-12-31 22:18:15', 'System outage or incident'),
(7, NULL, NULL, NULL, 'Service request', 'service-request', '2018-12-31 22:18:31', '2018-12-31 22:18:31', 'General request from a user for a product or service'),
(8, NULL, NULL, NULL, 'Change', 'change', '2018-12-31 22:18:45', '2018-12-31 22:18:45', 'Rollout of new technologies or solutions'),
(9, NULL, NULL, NULL, 'Problem', 'problem', '2018-12-31 22:18:56', '2018-12-31 22:18:56', 'Track underlying causes of incidents')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
