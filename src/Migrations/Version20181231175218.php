<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181231175218 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE gallery (id INT AUTO_INCREMENT NOT NULL, owner INT DEFAULT NULL, name VARCHAR(255) NOT NULL, context VARCHAR(64) NOT NULL, default_format VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_472B783ACF60E67C (owner), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, gallery INT DEFAULT NULL, owner INT DEFAULT NULL, updater INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_2FB3D0EE472B783A (gallery), INDEX IDX_2FB3D0EECF60E67C (owner), INDEX IDX_2FB3D0EE324F23A6 (updater), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_project (project_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_77BECEE4166D1F9C (project_id), INDEX IDX_77BECEE4A76ED395 (user_id), PRIMARY KEY(project_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE issue (id INT AUTO_INCREMENT NOT NULL, project INT DEFAULT NULL, version INT DEFAULT NULL, gallery INT DEFAULT NULL, assign INT DEFAULT NULL, owner INT DEFAULT NULL, updater INT DEFAULT NULL, summary VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, estimate INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_12AD233E2FB3D0EE (project), INDEX IDX_12AD233EBF1CD3C3 (version), INDEX IDX_12AD233E472B783A (gallery), INDEX IDX_12AD233E7222A9A1 (assign), INDEX IDX_12AD233ECF60E67C (owner), INDEX IDX_12AD233E324F23A6 (updater), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gallery_has_media (id INT AUTO_INCREMENT NOT NULL, gallery_id INT DEFAULT NULL, media_id INT DEFAULT NULL, position INT NOT NULL, enabled TINYINT(1) NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_CD2356644E7AF8F (gallery_id), INDEX IDX_CD235664EA9FDD75 (media_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, owner INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, enabled TINYINT(1) NOT NULL, provider_name VARCHAR(255) NOT NULL, provider_status INT NOT NULL, provider_reference VARCHAR(255) NOT NULL, provider_metadata JSON DEFAULT NULL, width INT DEFAULT NULL, height INT DEFAULT NULL, length NUMERIC(10, 0) DEFAULT NULL, content_type VARCHAR(255) DEFAULT NULL, content_size INT DEFAULT NULL, copyright VARCHAR(255) DEFAULT NULL, author_name VARCHAR(255) DEFAULT NULL, context VARCHAR(64) DEFAULT NULL, cdn_is_flushable TINYINT(1) DEFAULT NULL, cdn_flush_identifier VARCHAR(64) DEFAULT NULL, cdn_flush_at DATETIME DEFAULT NULL, cdn_status INT DEFAULT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_6A2CA10CCF60E67C (owner), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE issue_status (id INT AUTO_INCREMENT NOT NULL, project INT DEFAULT NULL, owner INT DEFAULT NULL, updater INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_89F0EF3B2FB3D0EE (project), INDEX IDX_89F0EF3BCF60E67C (owner), INDEX IDX_89F0EF3B324F23A6 (updater), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE version (id INT AUTO_INCREMENT NOT NULL, project INT DEFAULT NULL, owner INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_BF1CD3C32FB3D0EE (project), INDEX IDX_BF1CD3C3CF60E67C (owner), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE issue_type (id INT AUTO_INCREMENT NOT NULL, project INT DEFAULT NULL, owner INT DEFAULT NULL, updater INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_D4399FE52FB3D0EE (project), INDEX IDX_D4399FE5CF60E67C (owner), INDEX IDX_D4399FE5324F23A6 (updater), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE worklog (id INT AUTO_INCREMENT NOT NULL, issue INT DEFAULT NULL, assign INT DEFAULT NULL, owner INT DEFAULT NULL, updater INT DEFAULT NULL, start DATETIME NOT NULL, work_time INT NOT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_524AFE2E12AD233E (issue), INDEX IDX_524AFE2E7222A9A1 (assign), INDEX IDX_524AFE2ECF60E67C (owner), INDEX IDX_524AFE2E324F23A6 (updater), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE issue_priority (id INT AUTO_INCREMENT NOT NULL, owner INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_B50EF68CCF60E67C (owner), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE gallery ADD CONSTRAINT FK_472B783ACF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE472B783A FOREIGN KEY (gallery) REFERENCES gallery (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EECF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
        $this->addSql('ALTER TABLE user_project ADD CONSTRAINT FK_77BECEE4166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_project ADD CONSTRAINT FK_77BECEE4A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E2FB3D0EE FOREIGN KEY (project) REFERENCES project (id)');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233EBF1CD3C3 FOREIGN KEY (version) REFERENCES version (id)');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E472B783A FOREIGN KEY (gallery) REFERENCES gallery (id)');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E7222A9A1 FOREIGN KEY (assign) REFERENCES users (id)');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233ECF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
        $this->addSql('ALTER TABLE gallery_has_media ADD CONSTRAINT FK_CD2356644E7AF8F FOREIGN KEY (gallery_id) REFERENCES gallery (id)');
        $this->addSql('ALTER TABLE gallery_has_media ADD CONSTRAINT FK_CD235664EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10CCF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE issue_status ADD CONSTRAINT FK_89F0EF3B2FB3D0EE FOREIGN KEY (project) REFERENCES project (id)');
        $this->addSql('ALTER TABLE issue_status ADD CONSTRAINT FK_89F0EF3BCF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE issue_status ADD CONSTRAINT FK_89F0EF3B324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
        $this->addSql('ALTER TABLE version ADD CONSTRAINT FK_BF1CD3C32FB3D0EE FOREIGN KEY (project) REFERENCES project (id)');
        $this->addSql('ALTER TABLE version ADD CONSTRAINT FK_BF1CD3C3CF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE issue_type ADD CONSTRAINT FK_D4399FE52FB3D0EE FOREIGN KEY (project) REFERENCES project (id)');
        $this->addSql('ALTER TABLE issue_type ADD CONSTRAINT FK_D4399FE5CF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE issue_type ADD CONSTRAINT FK_D4399FE5324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
        $this->addSql('ALTER TABLE worklog ADD CONSTRAINT FK_524AFE2E12AD233E FOREIGN KEY (issue) REFERENCES issue (id)');
        $this->addSql('ALTER TABLE worklog ADD CONSTRAINT FK_524AFE2E7222A9A1 FOREIGN KEY (assign) REFERENCES users (id)');
        $this->addSql('ALTER TABLE worklog ADD CONSTRAINT FK_524AFE2ECF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE worklog ADD CONSTRAINT FK_524AFE2E324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
        $this->addSql('ALTER TABLE issue_priority ADD CONSTRAINT FK_B50EF68CCF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE472B783A');
        $this->addSql('ALTER TABLE issue DROP FOREIGN KEY FK_12AD233E472B783A');
        $this->addSql('ALTER TABLE gallery_has_media DROP FOREIGN KEY FK_CD2356644E7AF8F');
        $this->addSql('ALTER TABLE user_project DROP FOREIGN KEY FK_77BECEE4166D1F9C');
        $this->addSql('ALTER TABLE issue DROP FOREIGN KEY FK_12AD233E2FB3D0EE');
        $this->addSql('ALTER TABLE issue_status DROP FOREIGN KEY FK_89F0EF3B2FB3D0EE');
        $this->addSql('ALTER TABLE version DROP FOREIGN KEY FK_BF1CD3C32FB3D0EE');
        $this->addSql('ALTER TABLE issue_type DROP FOREIGN KEY FK_D4399FE52FB3D0EE');
        $this->addSql('ALTER TABLE worklog DROP FOREIGN KEY FK_524AFE2E12AD233E');
        $this->addSql('ALTER TABLE gallery_has_media DROP FOREIGN KEY FK_CD235664EA9FDD75');
        $this->addSql('ALTER TABLE issue DROP FOREIGN KEY FK_12AD233EBF1CD3C3');
        $this->addSql('DROP TABLE gallery');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE user_project');
        $this->addSql('DROP TABLE issue');
        $this->addSql('DROP TABLE gallery_has_media');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE issue_status');
        $this->addSql('DROP TABLE version');
        $this->addSql('DROP TABLE issue_type');
        $this->addSql('DROP TABLE worklog');
        $this->addSql('DROP TABLE issue_priority');
    }
}
