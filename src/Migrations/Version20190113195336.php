<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190113195336 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_has_media (id INT AUTO_INCREMENT NOT NULL, project_id INT DEFAULT NULL, media_id INT DEFAULT NULL, owner INT DEFAULT NULL, updater INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_8099F465166D1F9C (project_id), INDEX IDX_8099F465EA9FDD75 (media_id), INDEX IDX_8099F465CF60E67C (owner), INDEX IDX_8099F465324F23A6 (updater), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE issue_has_media (id INT AUTO_INCREMENT NOT NULL, issue_id INT DEFAULT NULL, media_id INT DEFAULT NULL, owner INT DEFAULT NULL, updater INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_D07663F85E7AA58C (issue_id), INDEX IDX_D07663F8EA9FDD75 (media_id), INDEX IDX_D07663F8CF60E67C (owner), INDEX IDX_D07663F8324F23A6 (updater), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_has_media ADD CONSTRAINT FK_8099F465166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_has_media ADD CONSTRAINT FK_8099F465EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE project_has_media ADD CONSTRAINT FK_8099F465CF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE project_has_media ADD CONSTRAINT FK_8099F465324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
        $this->addSql('ALTER TABLE issue_has_media ADD CONSTRAINT FK_D07663F85E7AA58C FOREIGN KEY (issue_id) REFERENCES issue (id)');
        $this->addSql('ALTER TABLE issue_has_media ADD CONSTRAINT FK_D07663F8EA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE issue_has_media ADD CONSTRAINT FK_D07663F8CF60E67C FOREIGN KEY (owner) REFERENCES users (id)');
        $this->addSql('ALTER TABLE issue_has_media ADD CONSTRAINT FK_D07663F8324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE project_has_media');
        $this->addSql('DROP TABLE issue_has_media');
    }
}
