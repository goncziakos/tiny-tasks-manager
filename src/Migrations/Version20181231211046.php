<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181231211046 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE issue ADD issue_type INT DEFAULT NULL, ADD status INT DEFAULT NULL, ADD priority INT DEFAULT NULL');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233ED4399FE5 FOREIGN KEY (issue_type) REFERENCES issue_type (id)');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E7B00651C FOREIGN KEY (status) REFERENCES issue_status (id)');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E62A6DC27 FOREIGN KEY (priority) REFERENCES issue_priority (id)');
        $this->addSql('CREATE INDEX IDX_12AD233ED4399FE5 ON issue (issue_type)');
        $this->addSql('CREATE INDEX IDX_12AD233E7B00651C ON issue (status)');
        $this->addSql('CREATE INDEX IDX_12AD233E62A6DC27 ON issue (priority)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE issue DROP FOREIGN KEY FK_12AD233ED4399FE5');
        $this->addSql('ALTER TABLE issue DROP FOREIGN KEY FK_12AD233E7B00651C');
        $this->addSql('ALTER TABLE issue DROP FOREIGN KEY FK_12AD233E62A6DC27');
        $this->addSql('DROP INDEX IDX_12AD233ED4399FE5 ON issue');
        $this->addSql('DROP INDEX IDX_12AD233E7B00651C ON issue');
        $this->addSql('DROP INDEX IDX_12AD233E62A6DC27 ON issue');
        $this->addSql('ALTER TABLE issue DROP issue_type, DROP status, DROP priority');
    }
}
