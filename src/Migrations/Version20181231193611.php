<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181231193611 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE gallery ADD updater INT DEFAULT NULL');
        $this->addSql('ALTER TABLE gallery ADD CONSTRAINT FK_472B783A324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_472B783A324F23A6 ON gallery (updater)');
        $this->addSql('ALTER TABLE media ADD updater INT DEFAULT NULL');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10C324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_6A2CA10C324F23A6 ON media (updater)');
        $this->addSql('ALTER TABLE version ADD updater INT DEFAULT NULL');
        $this->addSql('ALTER TABLE version ADD CONSTRAINT FK_BF1CD3C3324F23A6 FOREIGN KEY (updater) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_BF1CD3C3324F23A6 ON version (updater)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE gallery DROP FOREIGN KEY FK_472B783A324F23A6');
        $this->addSql('DROP INDEX IDX_472B783A324F23A6 ON gallery');
        $this->addSql('ALTER TABLE gallery DROP updater');
        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10C324F23A6');
        $this->addSql('DROP INDEX IDX_6A2CA10C324F23A6 ON media');
        $this->addSql('ALTER TABLE media DROP updater');
        $this->addSql('ALTER TABLE version DROP FOREIGN KEY FK_BF1CD3C3324F23A6');
        $this->addSql('DROP INDEX IDX_BF1CD3C3324F23A6 ON version');
        $this->addSql('ALTER TABLE version DROP updater');
    }
}
