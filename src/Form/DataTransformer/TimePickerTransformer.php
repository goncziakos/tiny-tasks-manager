<?php
/**
 * WorklogTimePickerTransformer.php
 *
 *
 * @package App\Form\DataTransformer
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2019.01.01.
 *
 */

namespace App\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;

class TimePickerTransformer implements DataTransformerInterface
{

    /**
     * @param mixed $value
     * @return mixed
     */
    public function transform($value)
    {
        return [
            'hours' => $value ? floor($value / 3600) : 0,
            'minutes' => $value ? $value % 3600 / 60 : 0
        ];
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function reverseTransform($value)
    {
        $seconds = 0;

        list('hours' => $hours, 'minutes' => $minutes) = $value;

        if ($hours) {
            $seconds += (int)$hours * 3600;
        }

        if ($minutes) {
            $seconds += (int)$minutes * 60;
        }

        return $seconds;
    }
}