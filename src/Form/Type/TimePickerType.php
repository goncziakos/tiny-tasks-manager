<?php
/**
 * WorklogTimePickerType.php
 *
 *
 * @package App\Form\Type
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2019.01.01.
 *
 */

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class TimePickerType extends AbstractType
{

    private $transformer;

    public function __construct(DataTransformerInterface $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addModelTransformer($this->transformer)
            ->add('hours', NumberType::class)
            ->add('minutes', NumberType::class);
    }
}