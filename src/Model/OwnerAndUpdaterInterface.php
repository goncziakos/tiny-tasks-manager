<?php
/**
 * OwnerAndUpdaterInterface.php
 *
 *
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2018.12.31.
 *
 */

namespace App\Model;

interface OwnerAndUpdaterInterface
{
    public function getCreatedAt(): ?\DateTimeInterface;

    public function setCreatedAt(\DateTimeInterface $createdAt): void;

    public function getUpdatedAt(): ?\DateTimeInterface;

    public function setUpdatedAt(\DateTimeInterface $updatedAt): void;

    public function getOwner(): UserInterface;

    public function setOwner(UserInterface $owner): void;

    public function getUpdater(): ?UserInterface;

    public function setUpdater(UserInterface $updater): void;
}