<?php

namespace App\Model;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

interface UserInterface extends BaseUserInterface
{
    public function getId();

    public function getEmail();

    public function getProjects(): Collection;

    public function setProjects(Collection $projects): void;
}