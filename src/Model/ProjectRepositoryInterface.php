<?php


namespace App\Model;


use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\QueryBuilder;

interface ProjectRepositoryInterface extends ObjectRepository
{
    public function setProjectUserFilter(QueryBuilder $queryBuilder, UserInterface $user): void;
}