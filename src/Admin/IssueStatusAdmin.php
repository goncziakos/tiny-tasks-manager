<?php
/**
 * ProjectAdmin.php
 *
 *
 * @package App\Admin
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2018.12.31.
 *
 */

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;

class IssueStatusAdmin extends AbstractAppAdmin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('project')
            ->add('name')
            ->add('slug')
            ->add('description')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('project')
            ->add('name')
            ->add('slug')
            ->add('description');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('project', ModelListType::class, [
                'required' => true,
                'btn_add' => 'link_add',
                'btn_edit' => 'link_edit',
                'btn_list' => 'link_list',
                'btn_delete' => false,
            ])
            ->add('name', null, [
                'required' => true,
            ])
            ->add('description', null, [
                'required' => false,
            ]);
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('project')
            ->add('name')
            ->add('slug')
            ->add('description');
    }

}