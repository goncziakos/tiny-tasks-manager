<?php
/**
 * ProjectAdmin.php
 *
 *
 * @package App\Admin
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2018.12.31.
 *
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class IssuePriorityAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('name')
            ->add('slug')
            ->add('description')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('slug')
            ->add('description');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name', null, [
                'required' => true,
            ])
            ->add('description', null, [
                'required' => false,
            ]);
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('name')
            ->add('slug')
            ->add('description');
    }
}