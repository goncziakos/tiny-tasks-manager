<?php
/**
 * ClientAdmin.php
 *
 *
 * @package App\Admin
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2019.04.27.
 *
 */

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\CollectionType;

class ClientAdmin extends AbstractAppAdmin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('name')
            ->add('slug')
            ->add('projects', null, [
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('slug')
            ->add('description');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name', null, [
                'required' => true,
            ])
            ->add('projects', ModelAutocompleteType::class, [
                'property' => 'name',
                'required' => false,
                'multiple' => true,
            ])
            ->add('description');
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('name')
            ->add('slug')
            ->add('projects')
            ->add('description')
        ;
    }
}