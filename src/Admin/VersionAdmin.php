<?php
/**
 * ProjectAdmin.php
 *
 *
 * @package App\Admin
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2018.12.31.
 *
 */

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class VersionAdmin extends AbstractAppAdmin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('project')
            ->add('name')
            ->add('slug')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('project')
            ->add('name')
            ->add('slug');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name', null, [
                'required' => true,
            ]);
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('project')
            ->add('name')
            ->add('slug');
    }

}