<?php
/**
 * ProjectAdmin.php
 *
 *
 * @package App\Admin
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2018.12.31.
 *
 */

namespace App\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Sonata\Form\Type\CollectionType;

class ProjectAdmin extends AbstractAppAdmin
{

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('name')
            ->add('slug')
            ->add('client')
            ->add('versions', null, [
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('users', null, [
                'route' => [
                    'name' => ''
                ]
            ])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('slug')
            ->add('client', ModelAutocompleteFilter::class, [], null, [
                'minimum_input_length' => 1,
                'property' => 'name',
                'multiple' => true
            ])
            ->add('description');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name', null, [
                'required' => true,
            ])
            ->add('client', ModelListType::class, [
                'required' => true,
                'btn_add' => 'link_add',
                'btn_edit' => 'link_edit',
                'btn_list' => 'link_list',
                'btn_delete' => false,
            ])
            ->add('versions', CollectionType::class, [
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->add('users', ModelAutocompleteType::class, [
                'property' => 'username',
                'required' => false,
                'multiple' => true,
            ])
            ->add('description', CKEditorType::class, [
                'required' => false,
                'config' => []
            ])
            ->add('projectHasMedia', CollectionType::class, [
                'label' => 'Attachments',
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ]);
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('name')
            ->add('slug')
            ->add('client')
            ->add('versions', null, [
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('users', null, [
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('description', null, ['safe' => true])
            ->add('projectHasMedia', null, ['template' => 'Admin/Issue/show.issue_has_media.html.twig'])
            ->add('updater')
            ->add('owner')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}