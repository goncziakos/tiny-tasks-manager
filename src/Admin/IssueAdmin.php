<?php
/**
 * ProjectAdmin.php
 *
 *
 * @package App\Admin
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2018.12.31.
 *
 */

namespace App\Admin;

use App\Entity\Issue;
use App\Form\Type\TimePickerType;
use App\Model\UserInterface;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Sonata\Form\Type\CollectionType;

class IssueAdmin extends AbstractAppAdmin
{

    public function getExportFields()
    {
        return [
            'id',
            'type',
            'status',
            'priority',
            'project',
            'summary',
            'assign',
            'estimate',
            'timeSpent',
            'owner',
            'createdAt',
            'description',
        ];
    }

    public function getDataSourceIterator()
    {
        /** @var \Sonata\Exporter\Source\DoctrineORMQuerySourceIterator $iterator */
        $iterator = parent::getDataSourceIterator();
        $iterator->setDateTimeFormat($this->getConfigurationPool()->getContainer()->getParameter('datetime_format'));
        return $iterator;
    }

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    public function getNewInstance()
    {
        /** @var Issue $subject */
        $subject = parent::getNewInstance();

        /** @var UserInterface $user */
        $user = $this->getUser();

        $subject->setAssign($user);

        return $subject;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('type', null, [
                'route' => ['name' => ''],
            ])
            ->add('project.client', null, [
                //'sortable' => true,
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('project', null, [
                //'sortable' => true,
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('parent', null, [
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('status', null, [
                'route' => ['name' => ''],
            ])
            ->add('summary')
            ->add('estimate', 'string', array('template' => 'Admin/Worklog/list_work_time.html.twig'))
            ->add('timeSpent', 'string', array('template' => 'Admin/Worklog/list_work_time.html.twig'))
            ->add('priority', null, [
                'route' => ['name' => ''],
            ])
            ->add('assign', null, [
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('owner', null, [
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('createdAt')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'new_worklog' => array(
                        'template' => 'Admin/Worklog/list_action_new_worklog.html.twig'
                    ),
                ),
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('project', ModelAutocompleteFilter::class, [], null, [
                'minimum_input_length' => 1,
                'property' => 'name',
                'multiple' => true
            ])
            ->add('project.client', ModelAutocompleteFilter::class, [], null, [
                'minimum_input_length' => 1,
                'property' => 'name',
                'multiple' => true
            ])
            ->add('assign', ModelAutocompleteFilter::class, [], null, [
                'minimum_input_length' => 1,
                'property' => 'username',
                'multiple' => true
            ])
            ->add('parent', ModelAutocompleteFilter::class, [], null, [
                'minimum_input_length' => 1,
                'property' => 'summary',
                'multiple' => true
            ])
            ->add('type', ModelAutocompleteFilter::class, [], null, [
                'minimum_input_length' => 1,
                'property' => 'name',
                'multiple' => true
            ])
            ->add('status', ModelAutocompleteFilter::class, [], null, [
                'minimum_input_length' => 1,
                'property' => 'name',
                'multiple' => true
            ])
            ->add('priority', null, [
            ], null, [
                'multiple' => true
            ])
            ->add('summary')
            ->add('description');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('assign', ModelListType::class, [
                'required' => true,
                'btn_add' => 'link_add',
                'btn_edit' => 'link_edit',
                'btn_list' => 'link_list',
                'btn_delete' => 'link_delete',
            ])
            ->add('project', ModelListType::class, [
                'required' => true,
                'btn_add' => 'link_add',
                'btn_edit' => 'link_edit',
                'btn_list' => 'link_list',
                'btn_delete' => false,
            ])
            ->add('version', ModelListType::class, [
                'required' => false,
                'btn_add' => 'link_add',
                'btn_edit' => 'link_edit',
                'btn_list' => 'link_list',
                'btn_delete' => 'link_delete',
            ])
            ->add('parent', ModelListType::class, [
                'required' => false,
                'btn_add' => 'link_add',
                'btn_edit' => 'link_edit',
                'btn_list' => 'link_list',
                'btn_delete' => 'link_delete',
            ])
            ->add('type', null, [
                'required' => true,
            ])
            ->add('status', null, [
                'required' => true,
            ])
            ->add('priority', null, [
                'required' => true,
            ])
            ->add('summary', null, [
                'required' => true,
            ])
            ->add('estimate', TimePickerType::class, [
                'required' => true,
            ])
            ->add('description', CKEditorType::class, [
                'required' => false,
                'config' => []
            ])
            ->add('issueHasMedia', CollectionType::class, [
                'label' => 'Attachments',
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ]);
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('project')
            ->add('type')
            ->add('status')
            ->add('parent')
            ->add('summary')
            ->add('slug')
            ->add('assign')
            ->add('estimate')
            ->add('timeSpent')
            ->add('issueHasMedia', null, ['template' => 'Admin/Issue/show.issue_has_media.html.twig'])
            ->add('updater')
            ->add('owner')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('description', null, array('safe' => true));
    }

}