<?php
/**
 * AbstractAppAdmin.php
 *
 *
 * @package App\Admin
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2019.01.04.
 *
 */

namespace App\Admin;

use App\Model\ProjectRepositoryInterface;
use App\Model\UserInterface;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

abstract class AbstractAppAdmin extends AbstractAdmin
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var ProjectRepositoryInterface
     */
    private $repository;

    public function createQuery($context = 'list')
    {
        /** @var ProxyQuery $query */
        $query = parent::createQuery($context);

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $query->getQueryBuilder();

        if ($this->getAuthorizationChecker()->isGranted('ROLE_SUPER_ADMIN') === false and $this->getAuthorizationChecker()->isGranted('ROLE_SUPER_MANAGER') === false) {
            $this->repository->setProjectUserFilter($queryBuilder, $this->getUser());
        }

        return $query;
    }

    public function addListMode(string $key, array $config, string $template)
    {
        $this->listModes[$key] = $config;
        $this->setTemplate('outer_list_rows_' . $key, $template);
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage): void
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getUser(): UserInterface
    {
        /** @var UserInterface $user */
        $user = $this->tokenStorage->getToken()->getUser();

        return $user;
    }

    /**
     * @return AuthorizationCheckerInterface
     */
    public function getAuthorizationChecker(): AuthorizationCheckerInterface
    {
        return $this->authorizationChecker;
    }

    /**
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function setAuthorizationChecker(AuthorizationCheckerInterface $authorizationChecker): void
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @return ProjectRepositoryInterface
     */
    public function getRepository(): ProjectRepositoryInterface
    {
        return $this->repository;
    }

    /**
     * @param ProjectRepositoryInterface $repository
     */
    public function setRepository(ProjectRepositoryInterface $repository): void
    {
        $this->repository = $repository;
    }
}