<?php

namespace App\Entity;

use App\Model\OwnerAndUpdaterInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\MediaBundle\Model\GalleryInterface;
use App\Model\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project implements OwnerAndUpdaterInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var Client|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="projects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var GalleryInterface|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Gallery")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gallery", referencedColumnName="id")
     * })
     */
    private $gallery;

    /**
     * @var ProjectHasMedia[]|Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="ProjectHasMedia",
     *     orphanRemoval=true,
     *     mappedBy="project",
     *     cascade={"all"}
     * )
     */
    private $projectHasMedia;

    /**
     * @var Version[]|Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="Version",
     *     orphanRemoval=true,
     *     mappedBy="project",
     *     cascade={"all"}
     * )
     *
     */
    private $versions;

    /**
     * @var UserInterface[]|Collection
     *
     * @ORM\ManyToMany(targetEntity="User", cascade={"persist"}, inversedBy="projects")
     * @ORM\JoinTable(name="user_project",
     *   joinColumns={
     *     @ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $users;

    /**
     * @var UserInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var UserInterface|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="updater", referencedColumnName="id")
     */
    private $updater;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true))
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->versions = new ArrayCollection();
        $this->projectHasMedia = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getGallery(): ?GalleryInterface
    {
        return $this->gallery;
    }

    public function setGallery(?GalleryInterface $gallery): void
    {
        $this->gallery = $gallery;
    }

    public function getOwner(): UserInterface
    {
        return $this->owner;
    }

    public function setOwner(UserInterface $owner): void
    {
        $this->owner = $owner;
    }

    public function getUpdater(): ?UserInterface
    {
        return $this->updater;
    }

    public function setUpdater(UserInterface $updater): void
    {
        $this->updater = $updater;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function setUsers(Collection $users): void
    {
        $this->users = $users;
    }

    public function addUser(UserInterface $user)
    {
        $this->users->add($user);
    }

    public function removeUser(UserInterface $user)
    {
        $this->users->removeElement($user);
    }

    public function getVersions(): Collection
    {
        return $this->versions;
    }

    public function addVersion(Version $version)
    {
        $this->versions->add($version);
        $version->setProject($this);
    }

    public function removeVersion(Version $version)
    {
        $this->versions->removeElement($version);
        $version->setProject(null);
    }

    public function getProjectHasMedia(): Collection
    {
        return $this->projectHasMedia;
    }

    public function setProjectHasMedia(Collection $projectHasMedias): void
    {
        foreach ($projectHasMedias as $projectHasMedia) {
            $projectHasMedia->setProject($this);
        }
        $this->projectHasMedia = $projectHasMedias;
    }

    public function addProjectHasMedia(ProjectHasMedia $projectHasMedia)
    {
        $this->projectHasMedia->add($projectHasMedia);
        $projectHasMedia->setProject($this);
    }

    public function removeProjectHasMedia(ProjectHasMedia $attachment)
    {
        $this->projectHasMedia->removeElement($attachment);
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): void
    {
        $this->client = $client;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

}
