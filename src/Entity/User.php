<?php

namespace App\Entity;

use App\Model\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="UserGroup")
     * @ORM\JoinTable(name="user_group")
     */
    protected $groups;

    /**
     * @var string|null
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     */
    protected $facebook_id;

    /**
     * @var string|null
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     */
    protected $facebook_access_token;

    /**
     * @var string|null
     * @ORM\Column(name="google_id", type="string", length=255, nullable=true)
     */
    protected $google_id;

    /**
     * @var string|null
     * @ORM\Column(name="google_access_token", type="string", length=255, nullable=true)
     */
    protected $google_access_token;

    /**
     * @var Project[]|Collection
     * @ORM\ManyToMany(targetEntity="Project", mappedBy="users")
     */
    protected $projects;

    public function __construct()
    {
        parent::__construct();
        $this->projects = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getFacebookId(): ?string
    {
        return $this->facebook_id;
    }

    /**
     * @param string|null $facebook_id
     */
    public function setFacebookId(?string $facebook_id): void
    {
        $this->facebook_id = $facebook_id;
    }

    /**
     * @return string|null
     */
    public function getFacebookAccessToken(): ?string
    {
        return $this->facebook_access_token;
    }

    /**
     * @param string|null $facebook_access_token
     */
    public function setFacebookAccessToken(?string $facebook_access_token): void
    {
        $this->facebook_access_token = $facebook_access_token;
    }

    /**
     * @return string|null
     */
    public function getGoogleId(): ?string
    {
        return $this->google_id;
    }

    /**
     * @param string|null $google_id
     */
    public function setGoogleId(?string $google_id): void
    {
        $this->google_id = $google_id;
    }

    /**
     * @return string|null
     */
    public function getGoogleAccessToken(): ?string
    {
        return $this->google_access_token;
    }

    /**
     * @param string|null $google_access_token
     */
    public function setGoogleAccessToken(?string $google_access_token): void
    {
        $this->google_access_token = $google_access_token;
    }

    /**
     * @return Project[]|Collection
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    /**
     * @param Project[]|Collection $projects
     */
    public function setProjects(Collection $projects): void
    {
        $this->projects = $projects;
    }

}
