<?php

/**
 * GalleryHasMedia
 *
 * @package MediaBundle
 * @author goncziakos@gmail.com
 * @since: 2018.10.18. 22:34
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\MediaBundle\Entity\BaseGalleryHasMedia;
use Sonata\MediaBundle\Model\GalleryInterface;
use Sonata\MediaBundle\Model\MediaInterface;

/**
 * GalleryHasMedia
 *
 * @ORM\Entity
 * @ORM\Table(name="gallery_has_media")
 */
class GalleryHasMedia extends BaseGalleryHasMedia
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var GalleryInterface|null
     *
     * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="galleryHasMedias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gallery_id", referencedColumnName="id")
     * })
     */
    protected $gallery;

    /**
     * @var MediaInterface|null
     *
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="galleryHasMedias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     * })
     */
    protected $media;

    public function getId()
    {
        return $this->id;
    }

}