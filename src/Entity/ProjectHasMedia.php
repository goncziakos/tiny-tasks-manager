<?php
/**
 * ProjectHasMedia.php
 *
 *
 * @package App\Entity
 * @author Gönczi Ákos <goncziakos@gmail.com>
 * @since 2019.01.13.
 *
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Model\OwnerAndUpdaterInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use App\Model\UserInterface;

/**
 * ProjectHasMedia
 *
 * @ORM\Entity
 * @ORM\Table(name="project_has_media")
 */
class ProjectHasMedia implements OwnerAndUpdaterInterface
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Project|null
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="projectHasMedia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    protected $project;

    /**
     * @var MediaInterface|null
     *
     * @ORM\ManyToOne(targetEntity="Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     * })
     */
    protected $media;

    /**
     * @var UserInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var UserInterface|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="updater", referencedColumnName="id")
     */
    private $updater;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true))
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): void
    {
        $this->project = $project;
    }

    public function getMedia(): ?MediaInterface
    {
        return $this->media;
    }

    public function setMedia(?MediaInterface $media): void
    {
        $this->media = $media;
    }


    public function getOwner(): UserInterface
    {
        return $this->owner;
    }

    public function setOwner(UserInterface $owner): void
    {
        $this->owner = $owner;
    }

    public function getUpdater(): ?UserInterface
    {
        return $this->updater;
    }

    public function setUpdater(UserInterface $updater): void
    {
        $this->updater = $updater;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function __toString()
    {
        return $this->getProject() . ' | ' . $this->getMedia()->getName();
    }
}