<?php

namespace App\Entity;

use App\Model\OwnerAndUpdaterInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\MediaBundle\Model\GalleryInterface;
use App\Model\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IssueRepository")
 */
class Issue implements OwnerAndUpdaterInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Issue", mappedBy="parent")
     */
    private $children;

    /**
     * @var Issue|null
     *
     * @ORM\ManyToOne(targetEntity="Issue", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var Project|null
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @var IssueType|null
     *
     * @ORM\ManyToOne(targetEntity="IssueType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="issue_type", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @var IssueStatus|null
     *
     * @ORM\ManyToOne(targetEntity="IssueStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status", referencedColumnName="id")
     * })
     */
    private $status;

    /**
     * @var IssuePriority|null
     *
     * @ORM\ManyToOne(targetEntity="IssuePriority")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="priority", referencedColumnName="id")
     * })
     */
    private $priority;

    /**
     * @var Version|null
     *
     * @ORM\ManyToOne(targetEntity="Version")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="version", referencedColumnName="id")
     * })
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Slug(fields={"summary"})
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $estimate;

    /**
     * @var GalleryInterface|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Gallery")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gallery", referencedColumnName="id")
     * })
     */
    private $gallery;

    /**
     * @var UserInterface|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="assign", referencedColumnName="id")
     */
    private $assign;

    /**
     * @var Collection|Version[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Worklog",
     *     orphanRemoval=true,
     *     mappedBy="issue",
     *     cascade={"all"}
     * )
     *
     */
    private $worklogs;

    /**
     * @var IssueHasMedia[]|Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="IssueHasMedia",
     *     orphanRemoval=true,
     *     mappedBy="issue",
     *     cascade={"all"}
     * )
     */
    private $issueHasMedia;

    /**
     * @var UserInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var UserInterface|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="updater", referencedColumnName="id")
     */
    private $updater;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->worklogs = new ArrayCollection();
        $this->issueHasMedia = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEstimate(): ?int
    {
        return $this->estimate;
    }

    public function setEstimate(?int $estimate): self
    {
        $this->estimate = $estimate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getAssign(): ?UserInterface
    {
        return $this->assign;
    }

    public function setAssign(?UserInterface $assign): self
    {
        $this->assign = $assign;

        return $this;
    }

    public function getOwner(): UserInterface
    {
        return $this->owner;
    }

    public function setOwner(UserInterface $owner): void
    {
        $this->owner = $owner;
    }

    public function getUpdater(): ?UserInterface
    {
        return $this->updater;
    }

    public function setUpdater(UserInterface $updater): void
    {
        $this->updater = $updater;
    }

    public function getGallery(): ?GalleryInterface
    {
        return $this->gallery;
    }

    public function setGallery(?GalleryInterface $gallery): void
    {
        $this->gallery = $gallery;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getVersion(): ?Version
    {
        return $this->version;
    }

    public function setVersion(?Version $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getType(): ?IssueType
    {
        return $this->type;
    }

    public function setType(?IssueType $type): void
    {
        $this->type = $type;
    }

    public function getStatus(): ?IssueStatus
    {
        return $this->status;
    }

    public function setStatus(?IssueStatus $status): void
    {
        $this->status = $status;
    }

    public function getPriority(): ?IssuePriority
    {
        return $this->priority;
    }

    public function setPriority(?IssuePriority $priority): void
    {
        $this->priority = $priority;
    }

    public function getWorklogs(): Collection
    {
        return $this->worklogs;
    }

    public function setWorklogs(Collection $worklogs): void
    {
        $this->worklogs = $worklogs;
    }

    public function getTimeSpent(): int
    {
        $time = 0;
        /** @var Worklog $worklog */
        foreach ($this->getWorklogs() as $worklog) {
            $time += $worklog->getWorkTime();
        }
        return $time;
    }

    /**
     * @return Collection
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @param Collection $children
     */
    public function setChildren(Collection $children): void
    {
        $this->children = $children;
    }

    /**
     * @return Issue|null
     */
    public function getParent(): ?Issue
    {
        return $this->parent;
    }

    /**
     * @param Issue|null $parent
     */
    public function setParent(?Issue $parent): void
    {
        $this->parent = $parent;
    }

    public function getIssueHasMedia(): Collection
    {
        return $this->issueHasMedia;
    }

    public function setIssueHasMedia(Collection $issueHasMedias): void
    {
        foreach ($issueHasMedias as $issueHasMedia) {
            $issueHasMedia->setIssue($this);
        }
        $this->issueHasMedia = $issueHasMedias;
    }

    public function addIssueHasMedia(IssueHasMedia $issueHasMedia)
    {
        $this->issueHasMedia->add($issueHasMedia);
        $issueHasMedia->setIssue($this);
    }

    public function removeIssueHasMedia(IssueHasMedia $issueHasMedia)
    {
        $this->issueHasMedia->removeElement($issueHasMedia);
    }

    public function __toString()
    {
        return (string)$this->getSummary();
    }
}
